package ru.rustore.unitysdk;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import ru.rustore.unitysdk.remoteconfigclient.RuStoreUnityRemoteConfigClient;

public class RuStoreRemoteConfigApplication extends Application {

	public final String APP_ID = "1dca9044-c204-47ea-9618-fafe3af30f1a";
	public final int UPDATE_TIME = 15;
	public final String ACCOUNT = "Auf";
	public final String LANGUAGE = "ru";

    @Override
    public void onCreate() {
		super.onCreate();
		
		SharedPreferences preferences = getSharedPreferences("rustore_shared_preferences", Context.MODE_PRIVATE);

		int updateTime = preferences.getInt("rustore_update_time", UPDATE_TIME);
		String updateBehaviour = preferences.getString("rustore_update_behaviour", "Actual");
		String account = preferences.getString("rustore_account", ACCOUNT);
		String language = preferences.getString("rustore_language", LANGUAGE);

		RuStoreUnityRemoteConfigClient.INSTANCE.setAccount(account);
		RuStoreUnityRemoteConfigClient.INSTANCE.setLanguage(language);
		RuStoreUnityRemoteConfigClient.INSTANCE.init(APP_ID, updateTime, updateBehaviour, null, null, getApplicationContext());
    }
}
