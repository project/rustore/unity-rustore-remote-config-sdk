package com.plugins.remoteconfigxample;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

public class RuStoreRemoteConfigAndroidUtils
{
	public void showToast(final Activity activity, String message)
	{
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			}
		});
	}
	
	public String GetStringSharedPreferences(final Application application, String storageName, String key, String defaultValue)
	{
		SharedPreferences preferences = application.getSharedPreferences(storageName, Context.MODE_PRIVATE);
		return preferences.getString(key, defaultValue);
	}
	
	public int GetIntSharedPreferences(final Application application, String storageName, String key, int defaultValue)
	{
		SharedPreferences preferences = application.getSharedPreferences(storageName, Context.MODE_PRIVATE);
		return preferences.getInt(key, defaultValue);
	}
	
	public void SetStringSharedPreferences(final Application application, String storageName, String key, String value)
	{
		SharedPreferences preferences = application.getSharedPreferences(storageName, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString(key, value);
		editor.apply();
	}
	
	public void SetIntSharedPreferences(final Application application, String storageName, String key, int value)
	{
		SharedPreferences preferences = application.getSharedPreferences(storageName, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putInt(key, value);
		editor.apply();
	}
}
