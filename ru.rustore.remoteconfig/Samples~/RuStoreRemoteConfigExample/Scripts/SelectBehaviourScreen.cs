using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Text.RegularExpressions;

namespace RuStore.RemoteConfigExample.UI {

    public class SelectBehaviourScreen : MonoBehaviour {

        [SerializeField]
        private ToggleGroup toggleGroup;

        [SerializeField]
        private Toggle actualToggle;

        [SerializeField]
        private Toggle defaultToggle;

        [SerializeField]
        private Toggle snapshotToggle;

        [SerializeField]
        private Button continueButton;

        [SerializeField]
        private InputField timeInputField;

        [SerializeField]
        private GameObject restartText;

        private Toggle startToggle = null;

        private const string storageName = "rustore_shared_preferences";
        private const string behaviourKey = "rustore_update_behaviour";
        private const string timeKey = "rustore_update_time";
        private const int timeDefault = 15;
        
        private int startTime = timeDefault;

        private AndroidUtils utils = new AndroidUtils();

        private Toggle[] toggles = { };

        void Start() {
            toggles = new Toggle[] { actualToggle, defaultToggle, snapshotToggle };

            var behaviour = utils.GetStringSharedPreferences(storageName, behaviourKey, string.Empty);
            switch (behaviour) {
                case "Actual":
                    actualToggle.SetIsOnWithoutNotify(true);
                    break;
                case "Default":
                    defaultToggle.SetIsOnWithoutNotify(true);
                    break;
                case "Snapshot":
                    snapshotToggle.SetIsOnWithoutNotify(true);
                    break;
                default:
                    actualToggle.SetIsOnWithoutNotify(true);
                    break;
            }

            startToggle = toggles.FirstOrDefault(t => t.isOn);
            startTime = utils.GetIntSharedPreferences(storageName, timeKey, timeDefault);

            timeInputField.SetTextWithoutNotify(startTime.ToString());

            UpdateContinueButtonState();
        }

        public void ActualChanged(bool value) {
            ChangedAction(actualToggle, "Actual");
        }

        public void DefaultChanged(bool value) {
            ChangedAction(defaultToggle, "Default");
        }

        public void SnapshotChanged(bool value) {
            ChangedAction(snapshotToggle, "Snapshot");
        }

        private void ChangedAction(Toggle toggle, string behaviour) {
            toggleGroup.allowSwitchOff = false;
            utils.SetStringSharedPreferences(storageName, behaviourKey, behaviour);

            UpdateContinueButtonState();
        }

        public void TimeEdit(string value) {
            string input = Regex.Replace(value, @"[^\d]", "");
            utils.SetIntSharedPreferences(storageName, timeKey, int.Parse(input));

            UpdateContinueButtonState();
        }

        private void UpdateContinueButtonState()
        {
            var toggle = toggles.FirstOrDefault(t => t.isOn);
            var time = timeInputField.text;

            continueButton.interactable = (toggle == startToggle) && (toggle != null) && (time == startTime.ToString());
            restartText.SetActive(!continueButton.interactable && toggle != null);
        }
    }
}
