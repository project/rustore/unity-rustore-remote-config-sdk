using UnityEngine;

namespace RuStore.RemoteConfigExample.UI {

    [CreateAssetMenu(menuName = "" + nameof(RemoteConfigExampleSettings))]
    public class RemoteConfigExampleSettings : ScriptableObject {

        public string account;
        public string language;
        public string configKey;
    }
}
