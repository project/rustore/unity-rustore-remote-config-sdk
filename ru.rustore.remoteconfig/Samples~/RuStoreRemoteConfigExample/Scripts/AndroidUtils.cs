using UnityEngine;

namespace RuStore.RemoteConfigExample.UI {

    public class AndroidUtils {

        private const string androidUtilsClassName = "com.plugins.remoteconfigxample.RuStoreRemoteConfigAndroidUtils";

        public string GetStringSharedPreferences(string storageName, string key, string defaultValue) {
            using (AndroidJavaObject applicationJava = GetApplicationJava())
            using (AndroidJavaObject utils = new AndroidJavaObject(androidUtilsClassName)) {
                return utils.Call<string>("GetStringSharedPreferences", applicationJava, storageName, key, defaultValue);
            }
        }

        public int GetIntSharedPreferences(string storageName, string key, int defaultValue) {
            using (AndroidJavaObject applicationJava = GetApplicationJava())
            using (AndroidJavaObject utils = new AndroidJavaObject(androidUtilsClassName)) {
                return utils.Call<int>("GetIntSharedPreferences", applicationJava, storageName, key, defaultValue);
            }
        }

        public void SetStringSharedPreferences(string storageName, string key, string value) {
            using (AndroidJavaObject applicationJava = GetApplicationJava())
            using (AndroidJavaObject utils = new AndroidJavaObject(androidUtilsClassName)) {
                utils.Call("SetStringSharedPreferences", applicationJava, storageName, key, value);
            }
        }

        public void SetIntSharedPreferences(string storageName, string key, int value) {
            using (AndroidJavaObject applicationJava = GetApplicationJava())
            using (AndroidJavaObject utils = new AndroidJavaObject(androidUtilsClassName)) {
                utils.Call("SetIntSharedPreferences", applicationJava, storageName, key, value);
            }
        }

        private AndroidJavaObject GetApplicationJava() {
            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity")) {
                return currentActivity.Call<AndroidJavaObject>("getApplication");
            }
        }
    }
}
