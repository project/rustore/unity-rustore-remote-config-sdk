using RuStore.Internal;
using System;
using UnityEngine;

namespace RuStore.RemoteConfig.Internal {

    public class GetRemoteConfigListener : ResponseListener<RemoteConfig> {

        private const string javaClassName = "ru.rustore.unitysdk.remoteconfigclient.callbacks.GetRemoteConfigListener";

        public GetRemoteConfigListener(Action<RuStoreError> onFailure, Action<RemoteConfig> onSuccess) : base(javaClassName, onFailure, onSuccess) {
        }

        protected override RemoteConfig ConvertResponse(AndroidJavaObject responseObject) {
            return new RemoteConfigImpl(responseObject);
        }
    }
}
