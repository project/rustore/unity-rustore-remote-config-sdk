using UnityEngine;
using RuStore.Internal;
using System;

namespace RuStore.RemoteConfig.Internal {

    public class RemoteConfigClientEventListener : AndroidJavaProxy {

        private const string javaClassName = "ru.rustore.unitysdk.remoteconfigclient.model.UnityRemoteConfigClientEventListener";

        private IRemoteConfigClientEventListener _listener;

        public RemoteConfigClientEventListener(IRemoteConfigClientEventListener listener) : base(javaClassName) {
            _listener = listener;
        }

        public void backgroundJobErrors(AndroidJavaObject exception) {
            CallbackHandler.AddCallback(() => {
                Func<AndroidJavaObject, RuStoreError> convert = (errorObject) => {
                    return errorObject != null ? ErrorDataConverter.ConvertError(errorObject) : null;
                };

                var error = new BackgroundConfigUpdateError() {
                    message = exception.Get<string>("message"),
                    cause = convert(exception.Get<AndroidJavaObject>("cause"))
                };

                _listener?.BackgroundJobErrors(error);
            });
        }

        public void firstLoadComplete() {
            CallbackHandler.AddCallback(() => {
                _listener?.FirstLoadComplete();
            });
        }

        public void initComplete() {
            CallbackHandler.AddCallback(() => {
                _listener?.InitComplete();
            });
        }

        public void memoryCacheUpdated() {
            CallbackHandler.AddCallback(() => {
                _listener?.MemoryCacheUpdated();
            });
        }

        public void persistentStorageUpdated() {
            CallbackHandler.AddCallback(() => {
                _listener?.PersistentStorageUpdated();
            });
        }

        public void remoteConfigNetworkRequestFailure(AndroidJavaObject errorObject) {
            var error = ErrorDataConverter.ConvertError(errorObject);
            CallbackHandler.AddCallback(() => {
                _listener?.RemoteConfigNetworkRequestFailure(error);
            });
        }
    }
}
