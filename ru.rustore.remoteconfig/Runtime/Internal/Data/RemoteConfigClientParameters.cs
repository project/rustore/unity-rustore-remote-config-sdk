﻿using UnityEngine;

namespace RuStore.RemoteConfig.Internal {
    public class RemoteConfigClientParameters : AndroidJavaProxy {

        private const string javaClassName = "ru.rustore.unitysdk.remoteconfigclient.model.RemoteConfigClientParameters";

        private string _appBuild;
        private string _appVersion;
        private string _deviceId;
        private string _deviceModel;
        private string _environment;
        private string _osVersion;

        public RemoteConfigClientParameters(RuStoreRemoteConfigClientSettings settings) : base(javaClassName) {
            _appBuild = string.IsNullOrEmpty(settings.appBuild) ? null : settings.appBuild;
            _appVersion = string.IsNullOrEmpty(settings.appVersion) ? null : settings.appVersion;
            _deviceId = string.IsNullOrEmpty(settings.deviceId) ? null : settings.deviceId;
            _deviceModel = string.IsNullOrEmpty(settings.deviceModel) ? null : settings.deviceModel;
            _environment = settings.environment == RuStoreRemoteConfigClientSettings.Environment.Default ? null : settings.environment.ToString();
            _osVersion = string.IsNullOrEmpty(settings.osVersion) ? null : settings.osVersion;
        }

        public string getAppBuild() {
            return _appBuild;
        }
        public string getAppVersion() {
            return _appVersion;
        }
        public string getDeviceId() {
            return _deviceId;
        }
        public string getDeviceModel() {
            return _deviceModel;
        }
        public string getEnvironment() {
            return _environment;
        }
        public string getOsVersion() {
            return _osVersion;
        }
    }
}