namespace RuStore.RemoteConfig {

    /// <summary>
    /// Содержит информацию об ошибке в работе фоновой синхронизации.
    /// </summary>
    public class BackgroundConfigUpdateError {

        /// <summary>
        /// Сообщение ошибки.
        /// </summary>
        public string message;
        
        /// <summary>
        /// Дополнительная информация об ошибке.
        /// </summary>
        public RuStoreError cause;
    }
}
