
using UnityEngine;

namespace RuStore.RemoteConfig.Internal {

    public class RemoteConfigImpl : RemoteConfig {

        private AndroidJavaObject _remoteConfig;

        public RemoteConfigImpl(AndroidJavaObject remoteConfig) {
            _remoteConfig = remoteConfig;
        }

        public override bool ContainsKey(string key) {
            return _remoteConfig.Call<bool>("containsKey", key);
        }

        public override bool GetBool(string key) {
            var value = _remoteConfig.Call<string>("getString", key);
            return bool.Parse(value);
        }

        public override double GetDouble(string key) {
            var value = _remoteConfig.Call<string>("getString", key);
            return double.Parse(value);
        }

        public override float GetFloat(string key) {
            var value = _remoteConfig.Call<string>("getString", key);
            return float.Parse(value);
        }

        public override int GetInt(string key) {
            var value = _remoteConfig.Call<string>("getString", key);
            return int.Parse(value);
        }

        public override long GetLong(string key)
        {
            var value = _remoteConfig.Call<string>("getString", key);
            return long.Parse(value);
        }

        public override string GetString(string key) {
            return _remoteConfig.Call<string>("getString", key);
        }
    }
}