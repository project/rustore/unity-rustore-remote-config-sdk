
namespace RuStore.RemoteConfig {

    /// <summary>
    /// Абстрактный класс текущего набора всех данных, полученных в зависимости от выбранной политики обновления при инициализации.
    /// </summary>
    public abstract class RemoteConfig {

        /// <summary>
        /// Метод проверяет наличие ключа в текущем экземпляре RemoteConfig.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Возвращает true, если параметр с указанным ключом присутствует в наборе данных, в противном случае — false.</returns>
        public abstract bool ContainsKey(string key);

        /// <summary>
        /// Приведение значения параметра к типу bool.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract bool GetBool(string key);

        /// <summary>
        /// Приведение значения параметра к типу int.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract int GetInt(string key);

        /// <summary>
        /// Приведение значения параметра к типу long.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract long GetLong(string key);

        /// <summary>
        /// Приведение значения параметра к типу string.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract string GetString(string key);

        /// <summary>
        /// Приведение значения параметра к типу double.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract double GetDouble(string key);

        /// <summary>
        /// Приведение значения параметра к типу float.
        /// </summary>
        /// <param name="key">Имя ключа.</param>
        /// <returns>Значение параметра.</returns>
        public abstract float GetFloat(string key);
    }
}
