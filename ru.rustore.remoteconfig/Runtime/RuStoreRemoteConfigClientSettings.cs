using UnityEngine;

namespace RuStore.RemoteConfig {

    /// <summary>
    /// Опциональные параметры инициализации через C#.
    /// </summary>
    public class RuStoreRemoteConfigClientSettings {

        /// <summary>
        /// Позволяет сравнивать Environment со значением, установленным в интерфейсе.
        /// </summary>
        public enum Environment {
            
            /// <summary>
            /// Значение по умолчанию.
            /// </summary>
            Default,

            /// <summary>
            /// Alpha.
            /// </summary>
            Alpha,

            /// <summary>
            /// Beta.
            /// </summary>
            Beta,

            /// <summary>
            /// Release.
            /// </summary>
            Release,
        }

        /// <summary>
        /// Уникальный идентификатор инструмента Remote Config.
        /// Доступен в консоли разработчика RuStore на странице создания параметров Remote Config.
        /// </summary>
        public string appId;

        /// <summary>
        /// Позволят сравнивать Account со значением, установленным в интерфейсе.
        /// </summary>
        public string account;

        /// <summary>
        /// Позволят сравнивать Language со значением, установленным в интерфейсе.
        /// По умолчанию Language не передается, в этом случае возвращается конфиг по умолчанию.
        /// Для передачи Language необходимо реализовать ConfigRequestParameterProvider.
        /// </summary>
        public string language;


        /// <summary>
        /// Позволят сравнивать AppBuild со значением, установленным в интерфейсе.
        /// </summary>
        public string appBuild;

        /// <summary>
        /// Позволят сравнивать AppVersion со значением, установленным в интерфейсе.
        /// </summary>
        public string appVersion;

        /// <summary>
        /// Позволят сравнивать DeviceId со значением, установленным в интерфейсе.
        /// </summary>
        public string deviceId;

        /// <summary>
        /// Позволят сравнивать DeviceModel со значением, установленным в интерфейсе.
        /// По умолчанию DeviceModel не передается, у этом случае возвращается конфиг по умолчанию.
        /// </summary>
        public string deviceModel;

        /// <summary>
        /// Позволят сравнивать OsVersion со значением, установленным в интерфейсе.
        /// По умолчанию OsVersion не передается, в этом случае возвращается конфиг по умолчанию.
        /// </summary>
        public string osVersion;

        /// <summary>
        /// Позволяет сравнивать Environment со значением, установленным в интерфейсе.
        /// Может принимать значения: Alpha, Beta, Release.
        /// Этот параметр удобно использовать для тестирования конфигурации на различных сборках приложения.
        /// </summary>
        public Environment environment = Environment.Default;
    }
}
