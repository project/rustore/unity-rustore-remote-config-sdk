﻿
namespace RuStore.RemoteConfig {

    public interface IRemoteConfig {

        public bool ContainsKey(string key);

        public bool GetBool(string key);
        public int GetInt(string key);
        public string GetString(string key);
        public double GetDouble(string key);
        public float GetFloat(string key);
    }
}