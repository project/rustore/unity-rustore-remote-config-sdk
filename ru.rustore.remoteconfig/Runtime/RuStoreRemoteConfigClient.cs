using UnityEngine;
using System;
using RuStore.RemoteConfig.Internal;

namespace RuStore.RemoteConfig {

    /// <summary>
    /// Класс реализует API для получения конфигурации данных с удаленного сервера.
    /// Инкапсулирует запрос конфигурации с сервера, кэширование, фоновое обновление.
    /// </summary>
    public class RuStoreRemoteConfigClient {

        /// <summary>
        /// Версия плагина.
        /// </summary>
        public static string PluginVersion = "8.0.0";

        private static RuStoreRemoteConfigClient _instance;
        private static bool _isInstanceInitialized;

        private bool _isInitialized;

        /// <summary>
        /// Возвращает true, если синглтон инициализирован, в противном случае — false.
        /// </summary>
        public bool IsInitialized => _isInitialized;
        private AndroidJavaObject _clientWrapper;

        /// <summary>
        /// Возвращает единственный экземпляр RuStoreRemoteConfigClient (реализация паттерна Singleton).
        /// Если экземпляр еще не создан, создает его.
        /// </summary>
        public static RuStoreRemoteConfigClient Instance {
            get {
                if (!_isInstanceInitialized) {
                    _isInstanceInitialized = true;
                    _instance = new RuStoreRemoteConfigClient();
                }
                return _instance;
            }
        }

        private RuStoreRemoteConfigClient() {
        }

        /// <summary>
        /// Выполняет инициализацию синглтона RuStoreRemoteConfigClient.
        /// Для работы в режиме UpdateBehaviour.Actual инициализация может быть выполнена без создания расширения класса Application.
        /// При этом параметр APP_ID должен быть передан из C#.
        /// </summary>
        /// <param name="settings">
        /// Объект класса RuStore.RemoteConfig.RuStoreRemoteConfigClientSettings.
        /// Содержит параметры инициализации для режима UpdateBehaviour.Actual.
        /// </param>
        /// <param name="eventListener">Объект класса, реализующего интерфейс RuStore.RemoteConfig.IRemoteConfigClientEventListener.</param>
        /// <returns>Возвращает true, если инициализация была успешно выполнена, в противном случае — false.</returns>
        public bool Init(RuStoreRemoteConfigClientSettings settings, IRemoteConfigClientEventListener eventListener = null) {
            if (_isInitialized) {
                Debug.LogError("Error: RuStore Remote Config is already initialized");
                return false;
            }

            if (Application.platform != RuntimePlatform.Android) {
                return false;
            }

            CallbackHandler.InitInstance();

            using (var clientJavaClass = new AndroidJavaClass("ru.rustore.unitysdk.remoteconfigclient.RuStoreUnityRemoteConfigClient")) {
                _clientWrapper = clientJavaClass.GetStatic<AndroidJavaObject>("INSTANCE");
            }

            if (!string.IsNullOrEmpty(settings.account)) {
                SetAccount(settings.account);
            }

            if (!string.IsNullOrEmpty(settings.language)) {
                SetLanguage(settings.language);
            }

            var parameters = new RemoteConfigClientParameters(settings);
            var listener = new RemoteConfigClientEventListener(eventListener);

            _clientWrapper.Call("init", settings.appId, 15, "Actual", parameters, listener);

            _isInitialized = true;
            return true;
        }

        /// <summary>
        /// Выполняет инициализацию синглтона RuStoreRemoteConfigClient.
        /// Инициализация Java-класса RuStoreUnityRemoteConfigClient с параметрами UpdateBehaviour.Default/UpdateBehaviour.Snapshot должна происходить в момент Application.onCreate(),
        /// т.к. при запуске фоновой синхронизации SDK должна быть проинициализирована.
        /// </summary>
        /// <param name="eventListener">Объект класса, реализующего интерфейс RuStore.RemoteConfig.IRemoteConfigClientEventListener.</param>
        /// <returns>Возвращает true, если инициализация была успешно выполнена, в противном случае — false.</returns>
        public bool Init(IRemoteConfigClientEventListener eventListener = null) {
            if (Application.platform != RuntimePlatform.Android) {
                return false;
            }

            CallbackHandler.InitInstance();

            using (var clientJavaClass = new AndroidJavaClass("ru.rustore.unitysdk.remoteconfigclient.RuStoreUnityRemoteConfigClient")) {
                _clientWrapper = clientJavaClass.GetStatic<AndroidJavaObject>("INSTANCE");
            }

            if (eventListener != null) {
                using (var listenerJavaClass = new AndroidJavaClass("ru.rustore.unitysdk.remoteconfigclient.RemoteConfigClientEventListenerDefault")) {
                    var instance = listenerJavaClass.GetStatic<AndroidJavaObject>("INSTANCE");
                    var listener = new RemoteConfigClientEventListener(eventListener);
                    instance.Call("setListener", listener);
                }
            }

            _isInitialized = true;
            return true;
        }

        /// <summary>
        /// Получение конфигурации данных в зависимости от выбранной политики обновления при инициализации.
        /// </summary>
        /// <param name="onFailure">
        /// Действие, выполняемое в случае ошибки.
        /// Возвращает объект RuStore.RuStoreError с информацией об ошибке.
        /// </param>
        /// <param name="onSuccess">
        /// Действие, выполняемое при успешном завершении операции.
        /// Возвращает объект RuStore.RemoteConfig.RemoteConfig с информцаией о текущем наборе данных.
        /// </param>
        public void GetRemoteConfig(Action<RuStoreError> onFailure, Action<RemoteConfig> onSuccess) {
            if (!IsPlatformSupported(onFailure)) {
                return;
            }

            var listener = new GetRemoteConfigListener(onFailure, onSuccess);
            _clientWrapper.Call("getRemoteConfig",  listener);
        }

        /// <summary>
        /// Устанавливает параметр Account, который может быть использован для получения заданной конфигурации.
        /// </summary>
        /// <param name="account">Значение параметра Account.</param>
        public void SetAccount(string account) {
            if (!IsPlatformSupported((error) => { })) {
                return;
            }

            _clientWrapper.Call("setAccount", account);
        }

        /// <summary>
        /// Устанавливает параметр Language, который может быть использован для получения заданной конфигурации.
        /// </summary>
        /// <param name="language">Значение параметра Language.</param>
        public void SetLanguage(string language) {
            if (!IsPlatformSupported((error) => { })) {
                return;
            }

            _clientWrapper.Call("setLanguage", language);
        }

        private bool IsPlatformSupported(Action<RuStoreError> onFailure) {
            if (Application.platform != RuntimePlatform.Android) {
                onFailure?.Invoke(new RuStoreError() {
                    name = "RuStoreRemoteConfigClientError",
                    description = "Unsupported platform"
                });
                return false;
            }

            return true;
        }
    }
}
