
namespace RuStore.RemoteConfig {

    /// <summary>
    /// Интерфейс слушателя колбэков обновлений конфигурации данных.
    /// </summary>
    public interface IRemoteConfigClientEventListener {

        /// <summary>
        /// Вызывается при окончании инциализации.
        /// </summary>
        public void InitComplete();

        /// <summary>
        /// Вызывается при ошибке сетевого запроса Remote Сonfig.
        /// </summary>
        /// <param name="error">Объект RuStore.RuStoreError с информацией об ошибке.</param>
        public void RemoteConfigNetworkRequestFailure(RuStoreError error);

        /// <summary>
        /// Возвращает ошибку фоновой работы.
        /// </summary>
        /// <param name="exception">Объект RuStore.RemoteConfig.BackgroundConfigUpdateError с информацией об ошибке.</param>
        public void BackgroundJobErrors(BackgroundConfigUpdateError exception);

        /// <summary>
        /// Вызывается при окончании первой загрузки.
        /// </summary>
        public void FirstLoadComplete();

        /// <summary>
        /// Вызывается при изменениях кэша в памяти.
        /// </summary>
        public void MemoryCacheUpdated();

        /// <summary>
        /// Вызывается при изменении постоянного хранилища.
        /// </summary>
        public void PersistentStorageUpdated();
    }
}
