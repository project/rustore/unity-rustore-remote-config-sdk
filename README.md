### Unity-плагин RuStore для работы с облачным сервисом конфигурации приложения

#### [🔗 Документация разработчика][10]

SDK Remote Config это облачный сервис, который позволяет изменять поведение и внешний вид вашего приложения, не требуя от пользователей загрузки обновления приложения. SDK инкапсулирует в себе запрос конфигурации с сервера, кэширование, фоновое обновление.

Репозиторий содержит плагины **RuStoreRemoteConfigClient** и **RuStoreCore**, а также демонстрационное приложение с примерами использования и настроек. Поддерживаются версии Unity 2022+.

#### Сборка примера приложения

Вы можете ознакомиться с демонстрационным приложением содержащим представление работы всех методов sdk:
- [remoteconfig_example](https://gitflic.ru/project/rustore/unity-rustore-remote-config-sdk/file?file=remoteconfig_example)

#### Установка плагина в свой проект

Доступные варианты установки:

- подключение UPM-пакета через **Package Manager**:
    - вариант **Add package from tarball...** — рекомендуемый способ установки, см. [README](https://gitflic.ru/project/rustore/unity-rustore-remote-config-sdk/file?file=upm_tgz);
    - вариант **Add package from git URL...** — для немедленного получения последних изменений, см. [README](https://gitflic.ru/project/rustore/unity-rustore-remote-config-sdk/file?file=ru.rustore.remoteconfig);
    - вариант **Add package from disk...** — при необходимости самостоятельных доработок, см. [README](https://gitflic.ru/project/rustore/unity-rustore-remote-config-sdk/file?file=android_libraries);
- подключение `unitypackage` через **Import Assets** — устаревший способ установки, см. [README](https://gitflic.ru/project/rustore/unity-rustore-remote-config-sdk/file?file=unitypackages).

#### История изменений

[CHANGELOG](CHANGELOG.md)

#### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы, распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](MIT-LICENSE.txt).

#### Техническая поддержка

Дополнительная помощь и инструкции доступны в [документации RuStore](https://www.rustore.ru/help/) и по электронной почте support@rustore.ru.

[10]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unity/8-0-0
