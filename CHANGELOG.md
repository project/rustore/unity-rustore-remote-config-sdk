## История изменений

### Release 8.0.0
- Версия SDK RemoteConfig 8.0.0.

### Release 7.0.0
- Версия SDK RemoteConfig 7.0.0.

### Release 6.1.0
- Версия SDK RemoteConfig 6.1.0.
- RuStoreSDK помещена в отдельную assembly.

### Release 6.0.0
- Версия SDK RemoteConfig 6.+.
- Изменена структура репозитория.
- Добавлен проект с исходным кодом пакетов `.aar`.

### Release 1.0.0
- Версия SDK RemoteConfig 1.0.0.
