using RuStore.RemoteConfig;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace RuStore.RemoteConfigExample.UI {

    public class RemoteConfigScreen : MonoBehaviour, IRemoteConfigClientEventListener {

        [SerializeField]
        private MessageBox _messageBox;

        [SerializeField]
        private InputField _accountInput;

        [SerializeField]
        private InputField _languageInput;

        [SerializeField]
        private RemoteConfigExampleSettings _settings;

        [SerializeField]
        private Button _initButton;

        private bool _isConfigInitialized;

        private const string storageName = "rustore_shared_preferences";
        private const string accountKey = "rustore_account";
        private const string languageKey = "rustore_language";

        private AndroidUtils utils = new AndroidUtils();

        void Start() {
            string account = utils.GetStringSharedPreferences(storageName, accountKey, _settings.account);
            string language = utils.GetStringSharedPreferences(storageName, languageKey, _settings.language);

            _accountInput.SetTextWithoutNotify(account);
            _languageInput.SetTextWithoutNotify(language);

            RuStoreRemoteConfigClient.Instance.Init(this);
        }

        public void InitComplete() {
            Debug.Log("RemoteConfigClient: init complete");
            _isConfigInitialized = true;
        }

        public void RemoteConfigNetworkRequestFailure(RuStoreError error) {
            Debug.Log("RemoteConfigClient: network request failure");
        }

        public void BackgroundJobErrors(BackgroundConfigUpdateError exception) {
            Debug.Log(string.Format("RemoteConfigClient: {0}", exception.message));
        }

        public void FirstLoadComplete() {
            Debug.Log("RemoteConfigClient: first load complete");
        }

        public void MemoryCacheUpdated() {
            Debug.Log("RemoteConfigClient: memory cache updated");
        }

        public void PersistentStorageUpdated() {
            Debug.Log("RemoteConfigClient: persistent storage updated");
        }

        public void OnError(RuStoreError error) {
            ShowMessage("Error", string.Format("{0}: {1}", error.name, error.description));
        }

        public void RequestRemoteConfig() {
            if (_isConfigInitialized) {
                RuStoreRemoteConfigClient.Instance.SetAccount(_accountInput.text);
                RuStoreRemoteConfigClient.Instance.GetRemoteConfig(
                    onFailure: OnError,
                    onSuccess: (config) => {
                        var text = config.ContainsKey(_settings.configKey) ? config.GetString(_settings.configKey) : "";
                        ShowMessage("Success", string.Format("{0}: {1}", _settings.configKey, text));
                    });
            }
        }

        public void SetParameters() {
            if (RuStoreRemoteConfigClient.Instance.IsInitialized) {
                RuStoreRemoteConfigClient.Instance.SetAccount(_accountInput.text);
                RuStoreRemoteConfigClient.Instance.SetLanguage(_languageInput.text);
            }
        }

        private void ShowMessage(string title, string message, Action onClose = null) {
            _messageBox.Show(
                title: title,
                message: message,
                onClose: onClose);
        }
    }
}
