### Unity-плагин RuStore для работы с облачным сервисом конфигурации приложения

#### [🔗 Документация разработчика][10]

#### Условия работы SDK

Для работы SDK необходимо соблюдение следующих условий.

- На устройстве пользователя установлено приложение RuStore.
- Пользователь авторизован в приложении RuStore.
- Пользователь и приложение не должны быть заблокированы в RuStore.

#### Подготовка требуемых параметров

Перед настройкой примера приложения необходимо подготовить следующие данные.

- `applicationId` — уникальный идентификатор приложения в системе Android в формате обратного доменного имени (пример: `ru.rustore.sdk.example`).
- `*.keystore` — файл ключа, который используется для [подписи и аутентификации Android-приложения](https://www.rustore.ru/help/developers/publishing-and-verifying-apps/app-publication/apk-signature/).
- `appId` — уникальный идентификатор инструмента **Remote Config**. Доступен в [консоли разработчика RuStore](https://console.rustore.ru/toolbox/tools) на странице [создания параметров](https://www.rustore.ru/help/developers/tools/remote-config/parameters) remote config.

#### Настройка примера приложения

1. Откройте проект **Unity** из папки `remoteconfig_example`.
2. Откройте файл настроек **RemoteConfigExampleSettings.assets** (**Assets / RuStoreRemoteConfigExample**).
3. В поле **App Id** укажите значение `appId` — уникальный идентификатор инструмента **Remote Config**.
4. В поле **Config Key** укажите имя [параметра](https://www.rustore.ru/help/developers/tools/remote-config/parameters) из консоли инструмента **Remote Config**.

	> ⚠️ Вы можете изменить список параметров в файле **RemoteConfigExampleSettings.assets** (**Assets / RuStoreRemoteConfigExample**).

5. В разделе **Publishing Settings** (**Edit → Project Settings → Player → Android Settings**) выберите вариант **Custom Keystore** и задайте параметры **Path / Password**, **Alias / Password** подготовленного файла `*.keystore`.
6. В разделе **Other Settings** (**Edit → Project Settings → Player → Android Settings**) настройте раздел **Identification**, отметив опцию **Override Default Package Name** и указав `applicationId` в поле **Package Name**.
7. Выполните сборку проекта командой **Build** (**File → Build Settings**) и проверьте работу приложения.

### Сценарий использования

#### Выбор модели обновления

Начальный экран приложения предлагает установить значение [UpdateBehaviour][20]. Смена модели обновления потребует перезапуска приложения.

![Выбор модели обновления](images/01_select_update_behaviour.png)

#### Получение конфигурации

Тап по кнопке `Get remote config` выполняет получение и отображение [конфигурации][30].

![Получение конфигурации](images/02_get_remote_config.png)

#### История изменений

[CHANGELOG](../CHANGELOG.md)

#### Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы, распространяется под лицензией MIT. Информация о лицензировании доступна в документе [MIT-LICENSE](../MIT-LICENSE.txt).

#### Техническая поддержка

Дополнительная помощь и инструкции доступны в [документациии RuStore](https://www.rustore.ru/help/) и по электронной почте support@rustore.ru.

[10]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unity/8-0-0
[20]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unity/8-0-0#updatebehaviour
[30]: https://www.rustore.ru/help/developers/tools/remote-config/sdk/unity/8-0-0#getconfig
