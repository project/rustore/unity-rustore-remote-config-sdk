package ru.rustore.unitysdk.remoteconfigclient.model;

public enum UnityUpdateBehaviour {
    Actual,
    Default,
    Snapshot
}
