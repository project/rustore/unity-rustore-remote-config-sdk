package ru.rustore.unitysdk.remoteconfigclient

import android.content.Context
import ru.rustore.sdk.remoteconfig.*
import ru.rustore.unitysdk.core.PlayerProvider
import ru.rustore.unitysdk.remoteconfigclient.callbacks.GetRemoteConfigListener
import ru.rustore.unitysdk.remoteconfigclient.model.RemoteConfigClientParameters
import ru.rustore.unitysdk.remoteconfigclient.model.UnityRemoteConfigClientEventListener
import ru.rustore.unitysdk.remoteconfigclient.model.UnityUpdateBehaviour
import kotlin.time.Duration

object RuStoreUnityRemoteConfigClient {
	private var isInitialized: Boolean = false

	fun init(appId: String, updateTime: Int, updateBehaviour: String, parameters: RemoteConfigClientParameters?, eventListener: UnityRemoteConfigClientEventListener?) {
		init(appId, updateTime, updateBehaviour, parameters, eventListener, null)
	}

	fun init(appId: String, updateTime: Int, updateBehaviour: String, parameters: RemoteConfigClientParameters?, eventListener: UnityRemoteConfigClientEventListener?, applicationContext: Context?) {
		if (isInitialized) return

		val builder = RemoteConfigClientBuilder(
			appId = AppId(appId),
			context = applicationContext ?: PlayerProvider.getCurrentActivity().application
		)

		eventListener?.let{
			RemoteConfigClientEventListenerDefault.setListener(eventListener)
		}
		builder.setRemoteConfigClientEventListener(RemoteConfigClientEventListenerDefault)
		builder.setConfigRequestParameterProvider(ConfigRequestParameterProviderDefault)

		parameters?.apply {
			appBuild?.let { builder.setAppBuild(AppBuild(it)) }
			appVersion?.let { builder.setAppVersion(AppVersion(it)) }
			deviceId?.let { builder.setDeviceId(DeviceId(it)) }
			deviceModel?.let { builder.setDevice(DeviceModel(it)) }
			environment?.let { builder.setEnvironment(Environment.valueOf(it)) }
			osVersion?.let { builder.setOsVersion(OsVersion(it)) }
		}

		val behaviour: UpdateBehaviour = when (UnityUpdateBehaviour.valueOf(updateBehaviour)) {
			UnityUpdateBehaviour.Actual -> UpdateBehaviour.Actual
			UnityUpdateBehaviour.Snapshot -> UpdateBehaviour.Snapshot(Duration.parse(updateTime.toString() + "m"))
			UnityUpdateBehaviour.Default -> UpdateBehaviour.Default(Duration.parse(updateTime.toString() + "m"))
		}

		builder.setUpdateBehaviour(behaviour).build().init()

		isInitialized = true
	}

	fun getRemoteConfig(listener: GetRemoteConfigListener) {
		RemoteConfigClient.instance.getRemoteConfig()
			.addOnSuccessListener { result -> listener.OnSuccess(result) }
			.addOnFailureListener { throwable -> listener.OnFailure(throwable) }
	}

	fun setAccount(value: String) {
		ConfigRequestParameterProviderDefault.setAccount(value)
	}

	fun getAccount(): String = ConfigRequestParameterProviderDefault.getAccount()

	fun setLanguage(value: String) {
		ConfigRequestParameterProviderDefault.setLanguage(value)
	}

	fun getLanguage(): String = ConfigRequestParameterProviderDefault.getLanguage()
}
