package ru.rustore.unitysdk.remoteconfigclient.model;

public interface RemoteConfigClientParameters {

    String getAppBuild();
    String getAppVersion();
    String getDeviceId();
    String getDeviceModel();
    String getEnvironment();
    String getOsVersion();
}
